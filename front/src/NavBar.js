import React, { Component} from 'react'
import { Navbar, NavItem, Grid, Row, Col, Nav, NavDropdown, MenuItem } from 'react-bootstrap'
import { IndexLinkContainer } from 'react-router-bootstrap'
import {NavLink} from 'react-router-dom'
import io from 'socket.io-client'
import Login from './users/Login'

export default class NavBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      labels: [],
      counts: [],
      user: {}
    }
    this.socket = io('localhost:8080/museum')
}

  updateUser = (newUser) => {
    this.setState({
      user: newUser
    })
    this.props.updateUserApp(newUser)
  }

  componentWillMount () {
    this.socket.emit('MUSEUM_PER_REGION')
    this.socket.on('SEND_MUSEUM_PER_REGION', regions => {
      let names = []

      regions.map(region => {
        names.push(region.region)
        return null
      })
      this.setState({
        isLoading: false,
        labels: names
      })
    })
  }

  render () {
    let listReg = this.state.isLoading ? <MenuItem>Wait</MenuItem> : this.state.labels.map((reg, index) => {
      let url = `/museum/region/${reg}`
      return (
        <RegLink key={index} keyEvent={index++} url={url} reg={reg} />
      )
    })

    let inscriptionDisplay = this.state.user.id ? null :  <Inscription />

    return (
      <Grid fluid>
        <Row>
          <Col>
            <Navbar inverse>
              <Navbar.Header>
                <Navbar.Brand>
                  <NavLink to='/'>
                    Dream Cities
                  </NavLink>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                <Nav>
                  <NavDropdown title='Musées' id={1} >
                    <IndexLinkContainer to='/museum'>
                      <MenuItem eventKey='1'>
                      Répartition par régions
                      </MenuItem>
                    </IndexLinkContainer>
                    <MenuItem divider />
                    {listReg}
                  </NavDropdown>
                  <IndexLinkContainer to='/favorites'>
                    <NavItem>
                   favoris
                    </NavItem>
                  </IndexLinkContainer>
                </Nav>
                {inscriptionDisplay}
                <Login updateUser={this.updateUser} />
              </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>
      </Grid>
    )
  }
}

class RegLink extends Component {
  render () {
    return (
      <IndexLinkContainer to={this.props.url}>
        <MenuItem eventKey={this.props.keyEvent}>
          {this.props.reg}
        </MenuItem>
      </IndexLinkContainer>
    )
  }
}

class Inscription extends Component {
  render () {
    return (
      <Nav pullRight>
        <IndexLinkContainer to='/inscription'>
          <NavItem>
            Inscription
          </NavItem>
        </IndexLinkContainer>
      </Nav>
    )
  }
}

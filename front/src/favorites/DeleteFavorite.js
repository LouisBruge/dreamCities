import React, { Component} from 'react'
import { Col, Button } from 'react-bootstrap'
import io from 'socket.io-client'

export default class DeleteFavorite extends Component {
  constructor (props) {
    super(props)
    this.socket = io('localhost:8080/favorites')
  }

  deleteTrigger (e) {
    e.preventDefault()
    this.socket.emit('DELETE_FAVORITE', { id: this.props.id })
    this.socket.on('DELETE_FAVORITE_OK', () => {
      console.log('the favorite has been deleted')
    })
  }
  render () {
    return (
      <Col>
        <Button onClick={this.deleteTrigger.bind(this)}> Suppression du compte </Button>
      </Col>
    )
  }
}

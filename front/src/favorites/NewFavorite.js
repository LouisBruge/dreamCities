import React, { Component } from 'react'
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap'
import io from 'socket.io-client'

export default class NewFavorite extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: this.props.user,
      data: '',
      url: ''
    }

    this.socket = io('localhost:8080/favorite')
  }

  handleSubmit (e) {
    e.preventDefault()
      this.socket.emit('NEW_FAVORITE', {
        user: this.state.user,
        data: this.state.data,
        url: this.state.url
      })
  }

  handleChange (event) {
    let field = event.target.name
    let value = event.target.value
    this.setState({
      [field]: value
    })
  }

  render () {
    return (
      <Grid fluid >
        <Row>
          <Col>
            <form onSubmit={this.handleSubmit.bind(this)} >
              <FormGroup>
                <ControlLabel>Data</ControlLabel>
                <FormControl type='number' value={this.state.value} name='data' placeholder='Entrer votre pseudo' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Url</ControlLabel>
                <FormControl type='url' value={this.state.value} name='url' placeholder='Entrer votre mot de passe' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <Button type='submit' >Submit</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

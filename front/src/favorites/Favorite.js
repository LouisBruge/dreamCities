import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import io from 'socket.io-client'

export default class favorite extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: '',
      data: '',
      url: '',
      isLoading: false
    }

    this.socket = io('localhost:8080/favorite')
  }

  componentWillMount () {
    this.socket.emit('GET_FAVORITE', { id: this.props.id })
    this.socket.on('SEND_FAVORITE', data => {
      this.socket.close()
      let favorite = data.data
      console.log(favorite)

      console.log(this.state)
      this.socket.on('disconnect', function () {
        console.log('disconnect')
      })
    })
  }

  render () {
    let data = this.state
    if (data.isLoading) {
      return (
        <Grid>
          <Row>
            <Col>
              <h1> {this.props.data} </h1>
              <p> {this.props.url} </p>
            </Col>
          </Row>
        </Grid>
      )
    } else {
      return null
    }
  }
}

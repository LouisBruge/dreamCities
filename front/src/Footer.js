import React, { Component} from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import './Footer.css'
export default class Footer extends Component {
  render () {
    return (
      <footer>
        <Grid>
          <Row>
            <Col xs={12} sm={6}>
              <h6> Confection </h6>
              <ul className='list-unstyled'>
                <li> L. Bruge </li>
                <li> Contact : <a href='mailto:louis.bruge@gmail.com'>louis.bruge@gmail.com</a></li>
                <li> Sources : <a href='https://gitlab.com/LouisBruge/dreamCities'>Gitlab</a></li>
              </ul>
            </Col>
            <Col xs={12} sm={6}>
              <h6> Sources </h6>
              <ul className='list-unstyled'>
                <li> <a href='https://www.data.gouv.fr/fr/datasets/liste-et-localisation-des-musees-de-france/'>data.gouv.fr</a> </li>
              </ul>
            </Col>
          </Row>
        </Grid>
      </footer>
    )
  }
}

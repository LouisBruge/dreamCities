import React, { Component } from 'react'
import { Row, Col, Thumbnail, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import io from 'socket.io-client'
import './SmallMuseum.css'

export default class SmallMuseum extends Component {
  constructor (props) {
    super(props)
    this.socket = io('localhost:8080/favorite')
    this.url = 'localhost:8080/museum'
  }

  handleClick (e) {
    e.preventDefault()
    if (this.props.addState) {
      this.socket.emit('NEW_FAVORITE', {
        user: 1,
        data: this.props.id,
        url: this.url
      })
    } else {
      this.socket.emit('DELETE_FAVORITE', {
        id: this.props.id_fav
      }, console.log('suppression'))
    }
  }

  render () {
    let url = `/museum/${this.props.id}`
    let buttonAction = this.props.addState ? 'Ajouter aux favorites' : 'Supprimer des favoris'
    return (
      <Col xs={12} sm={6} md={4} lg={3} >
        <Thumbnail src={this.props.image} alt='240x200' className='thumbnail' >
          <h3 className='text-center'>
            {this.props.name}
          </h3>
          <Row>
            <Col xs={10} xsOffset={1} >
              <address>
                {this.props.address}
                <br />
                {this.props.city}
              </address>
            </Col>
          </Row>
          <Row>
            <Col xs={6} sm={12} lg={6}>
              <Button onClick={this.handleClick.bind(this)} className='center-block' bsSize="small">
                {buttonAction}
              </Button>
            </Col>
            <Col xs={6} sm={12} lg={6}>
              <Link to={url}>
                <Button className='center-block' bsSize="small">
                  En savoir plus
                </Button>
              </Link>
            </Col>
          </Row>
        </Thumbnail>
      </Col>
    )
  }
}

import React, { Component } from 'react'
import io from 'socket.io-client'
import { Polar } from 'react-chartjs-2'
import { Grid, Row, Col } from 'react-bootstrap'
import ListMuseum from './ListMuseum'

export default class ResumePerRegion extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      labels: [],
      counts: []
    }
    this.socket = io('localhost:8080/museum')
  }

  componentWillMount () {
    this.socket.emit('MUSEUM_CITIES', { departement: this.props.match.params.depart }, console.log('send'))
    this.socket.on('SEND_MUSEUM_CITIES', cities => {
      console.log(cities)
      let counts = []
      let names = []

      cities.map(city => {
        counts.push(city.nb)
        names.push(city.city)
        return null
      })
      this.setState({
        isLoading: false,
        labels: names,
        counts: counts
      })
      console.log(this.state)
    })
  }

  render () {
    if (this.state.isLoading) {
      return (<div> Loading... </div>)
    } else {
      let data = {
        labels: this.state.labels,
        datasets: [{
          backgroundColor: ['#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF'],
          data: this.state.counts
        }]
      }

      return (
        <Grid fluid>
          <Row>
            <Col>
              <h2 className='text-center'> Répartition des musées par villes </h2>
              <Polar data={data} legend={{ display: false }} />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <h3 className='text-center'> {this.props.match.params.depart} </h3>
            </Col>
            <Col xs={12}>
              <ListMuseum req={'MUSEUM_DEPART'} otherParams={{ departement: this.props.match.params.depart }} res={'SEND_MUSEUM_DEPART'} />
            </Col>
          </Row>
        </Grid>
      )
    }
  }
}

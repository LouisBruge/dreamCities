import React, { Component } from 'react'
import { Grid, Row, Col, Button, ListGroup, ListGroupItem } from 'react-bootstrap'
import io from 'socket.io-client'

export default class Museum extends Component {
  constructor (props) {
    super(props)
    this.state = {
      museum: {},
      activities: []
    }
    this.socket = io('localhost:8080/museum')
    this.socketFav = io('localhost:8080/favorite')
    this.url = 'localhost:8080/museum'
  }

  fetchActivities (city) {
    let url = `http://localhost:8080/${city}/activities`
    fetch(url)
      .then(response => { return response.json() })
      .then(myJSON => {
        let results = myJSON.map(activity => {
          return activity.name
        })
        this.setState({
          activities: results
        })
      })
  }

  handleClick (e) {
    e.preventDefault()
    this.socketFav.emit('NEW_FAVORITE', {
      user: 1,
      data: this.props.match.params.id,
      url: this.url
    })
    console.log('ok')
  }

  sendCityName () {
    return this.state.museum.city ? this.state.museum.city : 'paris'
  }

  componentWillMount () {
    this.socket.emit('MUSEUM', { id: this.props.match.params.id })
    this.socket.on('SEND_MUSEUM', (museum) => {
      this.socket.close()
      this.setState({
        museum: museum
      })
      this.fetchActivities(museum.city)
    })
  }

  render () {
    if (this.state.museum.length !== 0 && this.state.activities.length !== undefined) {
      let museum = this.state.museum

      let activities = this.state.activities.map(activity => { return <ListGroupItem>{activity}</ListGroupItem> })

      return (
        <Grid fluid>
          <Row>
            <Col xs={12}>
              <h1 className='text-center'> {museum.name} </h1>
            </Col>
          </Row>
          <Row>
            <Col xs={10} xsOffset={1} sm={5} smOffset={1} >
              <h3 className='text-center'> Adresse </h3>
              <address>
                {museum.address} <br />
                {museum.code_postal} {museum.city} <br />
                {museum.departemant} - {museum.region}
              </address>
              <h3 class='text-center' > Contact </h3>
              <p> tel : +33 / 0 {museum.phone} </p>
              <p> {museum.web} </p>
            </Col>
            <Col xs={10} xsOffset={1} sm={5}>
              <h3 className='text-center' > Horaires </h3>
              <p className='text-justify' > {museum.schedule} </p>
              <h3 className='text-center'> Eventuelle fermeture annuelle </h3>
              <p className='text-justify'> {museum.annual_closure} </p>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button onClick={this.handleClick.bind(this)} className='center-block' >Ajouter aux favoris</Button>
            </Col>
          </Row>
          <Row>
            <Col xs={12} >
              <h4 className='text-center'> Activités à proximité </h4>
            </Col>
            <Col xs={10} xsOffset={1} >
              <ListGroup>
                {activities}
              </ListGroup>
            </Col>
          </Row>
        </Grid>
      )
    } else {
      return null
    }
  }
}

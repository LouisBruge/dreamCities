import React, { Component } from 'react'
import io from 'socket.io-client'
import { Doughnut } from 'react-chartjs-2'
import { Grid, Row, Col } from 'react-bootstrap'
import ListMuseum from './ListMuseum'

export default class ResumePerRegion extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      labels: [],
      counts: []
    }
    this.socket = io('localhost:8080/museum')
  }

  componentWillMount () {
    this.socket.emit('MUSEUM_REG_PER_DEPART', { region: this.props.match.params.reg })
    this.socket.on('SEND_MUSEUM_REG_PER_DEPART', departs => {
      let counts = []
      let names = []

      departs.map(depart => {
        counts.push(depart.nb)
        names.push(depart.departemant)
        return null
      })
      this.setState({
        isLoading: false,
        labels: names,
        counts: counts
      })
      console.log(this.state)
    })
  }

  render () {
    if (this.state.isLoading) {
      return (<div> Loading... </div>)
    } else {
      let data = {
        labels: this.state.labels,
        datasets: [{
          backgroundColor: ['#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF', '#FF0000FF', '#CCFF00FF', '#00FF66FF', '#0066FFFF', '#CC00FFFF'],
          data: this.state.counts
        }]
      }

      let listMuseumPerDept = this.state.labels.map(
        (dept, index) => {
          return (
            <Row>
              <Col xs={12}>
                <h3 className='text-center'> {dept} </h3>
              </Col>
              <Col xs={12}>
                <ListMuseum req={'MUSEUM_DEPART'} otherParams={{ departement: dept }} res={'SEND_MUSEUM_DEPART'} key={index} />
              </Col>
            </Row>
          )
        }
      )
      return (
        <Grid fluid>
          <Row>
            <Col>
              <h2 className='text-center'> Répartition des musées par départements </h2>
            </Col>
            <Col xs={10} xsOffset={1} >
              <Doughnut data={data} legend={{ display: true, position: 'right', fullWidth: false }} />
            </Col>
          </Row>
          {listMuseumPerDept}
        </Grid>
      )
    }
  }
}

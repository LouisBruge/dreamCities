import React, { Component } from 'react'
import { Grid, Row } from 'react-bootstrap'
import { CardColumns } from 'reactstrap'
import io from 'socket.io-client'
import SmallMuseum from './SmallMuseum'

export default class ListMuseum extends Component {
  constructor (props) {
    super(props)
    this.state = {
      museum: []
    }

    this.socket = io('localhost:8080/museum')
  }

  componentWillMount () {
    let url = `https://pixabay.com/api/?key=6801112-eebb97b4fd98f198e135091f3&q=history&orientation=horizontal&image_type=photo&per_page=200`

    fetch(url)
      .then(response => {
        return response.json()
      })
      .then(myJSON => {
        this.socket.emit(this.props.req, this.props.otherParams)
        this.socket.on(this.props.res, allMuseum => {
          let listMuseum = this.state.museum
          allMuseum.map(museum => {
            let i = Math.floor(Math.random() * myJSON.hits.length)
            museum.image = myJSON.hits[i].previewURL
            listMuseum.push(museum)
            return null
          })
          this.setState({
            museum: listMuseum
          })
        })
      })
  }

  render () {
    if (this.state.museum.length === 0) {
      return (<div> <p> No museum </p> </div>)
    } else {
      let list = this.state.museum.map((museum, index) => {
        return (
          <SmallMuseum name={museum.name} city={museum.city} id={museum.id} url={museum.url_api} address={museum.address} image={museum.image} addState key={index} />
        )
      })

      return (
        <Grid fluid>
          <Row>
            <CardColumns>
              {list}
            </CardColumns>
          </Row>

        </Grid>
      )
    }
  }
}

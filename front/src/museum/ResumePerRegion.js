import React, { Component } from 'react'
import io from 'socket.io-client'
import { HorizontalBar, Doughnut } from 'react-chartjs-2'
import { Grid, Row, Col } from 'react-bootstrap'
export default class ResumePerRegion extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      labels: [],
      counts: []
    }
    this.socket = io('localhost:8080/museum')
  }

  componentWillMount () {
    this.socket.emit('MUSEUM_PER_REGION')
    this.socket.on('SEND_MUSEUM_PER_REGION', regions => {
      let counts = []
      let names = []

      regions.map(region => {
        counts.push(region.nb)
        names.push(region.region)
        return null
      })
      this.setState({
        isLoading: false,
        labels: names,
        counts: counts
      })
      console.log(this.state)
    })
  }

  render () {
    if (this.state.isLoading) {
      return (<div> Loading... </div>)
    } else {
      let data = {
        labels: this.state.labels,
        datasets: [{
          backgroundColor: '#7B0CE8',
          data: this.state.counts
        }]
      }

      let fifthFirstRegion = {
        labels: this.state.labels.splice(0, 5),
        datasets: [{
          label: 'Nombre de musées par région',
          backgroundColor: ['#7B0CE8', '#0D03E8', '#1071FF', '#03C6E8', '#08FFB1'],
          data: this.state.counts.splice(0, 5)
        }]
      }

      return (
        <Grid>
          <Row>
            <Col xsHidden smHidden md={12}>
              <h2 className='text-center'> Répartition des musées selon les régions </h2>
              <HorizontalBar data={data} legend={{ display: false }} />
            </Col>
            <Col mdHidden lgHidden xlHidden>
              <h2 className='text-center'> Les cinqs régions où le nombre de musées est le plus élevé </h2>
              <Doughnut data={fifthFirstRegion} legend={{ display: true, position: 'bottom', fullWidth : false }} />
            </Col>
          </Row>
        </Grid>
      )
    }
  }
}

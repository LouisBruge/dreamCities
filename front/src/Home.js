import React, { Component} from 'react'
import { Grid, Row, Col, Jumbotron, Image, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default class Home extends Component {
  render () {
    return (
      <Grid fluid>
        <Row>
          <Col xs={12}>
            <Image src='https://picsum.photos/g/1920/1080' responsive />
          </Col>
          <Col xs={12}>
            <Jumbotron>
              <h1 className='text-center'> Welcome </h1>
              <p className='text-center'>
                À la recherche du meilleur  musée, consulter notre site
              </p>
              <Link to='/museum'>
                <Button className='center-block'> Consulter </Button>
              </Link>
            </Jumbotron>
            <Jumbotron>
              <h1 className='text-center'> Envie de sauvegarder vos meilleurs musées ? </h1>
              <p className='text-center'> Une simple inscription pour garder à tout jamais vos musées préférés </p>
              <Link to='/inscription'>
                <Button className='center-block'> Consulter </Button>
              </Link>

            </Jumbotron>
          </Col>
        </Row>
      </Grid>
    )
  }
}

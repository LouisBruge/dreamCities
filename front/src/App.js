import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import NavBar from './NavBar'
import Museum from './museum/museum'
import ListMuseum from './museum/ListMuseum'
import ResumePerDept from './museum/ResumePerDept'
import MuseumDepart from './museum/MuseumDepart'
import ResumePerRegion from './museum/ResumePerRegion'
import Home from './Home'
import Inscription from './users/Inscription'
import Footer from './Footer'
import FavoritesUser from './users/FavoritesUser'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {
        id: 1
      }
    }
  }

  updateUserApp = (user) => {
    this.setState({
      user: user
    })
  }

  render () {
    return (
      <Router>
        <div>
          <div>
            <NavBar updateUserApp={this.updateUserApp} />
          </div>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/museum' component={ResumePerRegion} />
            <Route exact path='/museums' component={ListMuseum} />
            <Route exact path='/museum/:id(\d+)' component={Museum} />
            <Route exact path='/museum/depart/:depart' component={MuseumDepart} />
            <Route exact path='/museum/region/:reg' component={ResumePerDept} />
            <Route exact path='/inscription' component={Inscription} />
            <Route exact path='/user' component={FavoritesUser} />
            <Route exact path='/favorites' component={(props) => <FavoritesUser {...props} id={1} />} />
          </Switch>
          <div>
            <Footer />
          </div>
        </div>
      </Router>
    )
  }
}

export default App

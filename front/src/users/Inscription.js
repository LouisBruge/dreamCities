import React, { Component } from 'react'
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button, Alert } from 'react-bootstrap'
import io from 'socket.io-client'

export default class Inscription extends Component {
  constructor (props) {
    super(props)
    this.state = {
      lastName: '',
      firstName: '',
      email: '',
      password: '',
      userName: '',
      status: null
    }

    this.socket = io('localhost:8080')
  }

  handleSubmit (e) {
    e.preventDefault()
    if (this.state.lastName.trim().length !== 0 && this.state.firstName.trim().length !== 0 && this.state.email.trim().length !== 0 && this.state.password.trim().length !== 0 && this.state.userName.trim().length !== 0) {
      console.log('envois en cours')
      this.socket.emit('NEW_USER', {
        lastName: this.state.lastName,
        firstName: this.state.firstName,
        email: this.state.email,
        password: this.state.password,
        userName: this.state.userName
      })
      this.socket.on('RECEIVE_NEW_USER', () => {
        this.socket.close()
        console.log('receive')
        this.setState({
          value: '',
          status: 'success'
        })
      })
      this.socket.on('USER_ALREADY_IN_THE_DB', () => {
        this.setState({
          status: 'alert'
        })
      })
    }
  }

  handleChange (event) {
    let field = event.target.name
    let value = event.target.value
    this.setState({
      [field]: value
    })
  }
  render () {
    return (
      <Grid fluid >
        <Row>
          <Col xs={12} sm={10} smOffset={1}>
            <SignUpAlert status={this.state.status} />
            <form onSubmit={this.handleSubmit.bind(this)} >
              <FormGroup>
                <ControlLabel>Pseudonyme</ControlLabel>
                <FormControl type='text' value={this.state.value} name='userName' placeholder='Entrer votre pseudo' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Prénom</ControlLabel>
                <FormControl type='text' value={this.state.value} name='firstName' placeholder='Entrer votre prenom' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Nom</ControlLabel>
                <FormControl type='text' value={this.state.value} name='lastName' placeholder='Entrer votre nom' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Adresse email</ControlLabel>
                <FormControl type='email' value={this.state.value} name='email' placeholder='Entrer votre adresse email' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Mot de passe</ControlLabel>
                <FormControl type='password' value={this.state.value} name='password' placeholder='Entrer votre mot de passe' onChange={this.handleChange.bind(this)} />
              </FormGroup>
              <Button type='submit' className='center-block' >Submit</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

class SignUpAlert extends Component {
  render () {
    if (this.props.status === 'succes') {
      return (
        <Alert bsStyle='success'>
        Merci de vous être inscrit
        </Alert>
      )
    } else if (this.props.status === 'alert') {
      return (
        <Alert bsStyle='danger'>
        Ce pseudo est déjà utilisé
        </Alert>
      )
    } else {
      return null
    }
  }
}

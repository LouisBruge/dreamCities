import React, { Component} from 'react'
import { Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap'
import io from 'socket.io-client'

export default class UpdateUser extends Component {
  constructor (props) {
    super(props)
    this.state = {
      id: '',
      lastName: '',
      firstName: '',
      email: '',
      password: '',
      userName: '',
      isInscription: false
    }

    this.socket = io('localhost:8080')
  }

  componentWillMount () {
    console.log(this.props.user)
    this.setState({
      id: this.props.user.id,
      lastName: this.props.user.lastName,
      firstName: this.props.user.firstName,
      email: this.props.user.email,
      password: this.props.user.password,
      userName: this.props.user.userName
    })
  }

  componentDidMount () {
    this.socket.on('UPDATE_USER_OK', () => {
      console.log('User has been updated')
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    if (this.state.lastName.trim().length !== 0 && this.state.firstName.trim().length !== 0 && this.state.email.trim().length !== 0 && this.state.password.trim().length !== 0 && this.state.userName.trim().length !== 0) {
      let socketValue = this.state.isInscription ? 'SEND_NEW_USER' : 'UPDATE_USER'
      this.socket.emit(socketValue, {
        id: this.state.id,
        lastName: this.state.lastName,
        firstName: this.state.firstName,
        email: this.state.email,
        password: this.state.password,
        userName: this.state.userName
      })
      console.log('ok')
    }
  }

  handleChange (event) {
    let field = event.target.name
    let value = event.target.value
    this.setState({
      [field]: value
    })
  }

  render () {
    return (
      <Col>
        <form onSubmit={this.handleSubmit.bind(this)} >
          <FormGroup>
            <ControlLabel>Pseudonyme</ControlLabel>
            <FormControl type='text' value={this.state.value} name='userName' placeholder='Entrer votre pseudo' onChange={this.handleChange.bind(this)} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Prénom</ControlLabel>
            <FormControl type='text' value={this.state.value} name='firstName' placeholder='Entrer votre prenom' onChange={this.handleChange.bind(this)} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Nom</ControlLabel>
            <FormControl type='text' value={this.state.value} name='lastName' placeholder='Entrer votre nom' onChange={this.handleChange.bind(this)} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Adresse email</ControlLabel>
            <FormControl type='email' value={this.state.value} name='email' placeholder='Entrer votre adresse email' onChange={this.handleChange.bind(this)} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Mot de passe</ControlLabel>
            <FormControl type='password' value={this.state.value} name='password' placeholder='Entrer votre mot de passe' onChange={this.handleChange.bind(this)} />
          </FormGroup>
          <Button type='submit' >Submit</Button>
        </form>
      </Col>)
  }
}

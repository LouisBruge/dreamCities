import React, { Component} from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import io from 'socket.io-client'
import UpdateUser from './UpdateUser'
import DeleteUser from './DeleteUser'

export default class User extends Component {
  constructor (props) {
    super(props)
    this.state = {
      id: '',
      userName: '',
      firstName: '',
      lastName: '',
      password: '',
      email: '',
      isLoading: true
    }
    this.socket = io('localhost:8080')
  }

  componentWillMount () {
    this.socket.emit('USER', { id: 1 }, console.log('ok'))
    this.socket.on('SEND_USER', (user) => {
      this.setState({
        id: user.id,
        userName: user.userName,
        firstName: user.firstName,
        lastName: user.lastName,
        password: user.password,
        email: user.email,
        isLoading: false
      })
    })
  }
  render () {
    if (this.state.isLoading) {
      return null
    } else {
      return (
        <Grid fluid>
          <Row>
            <Col>
              <h2> {this.state.userName} </h2>
              <p> {this.state.lastName.toUpperCase()} {this.state.firstName.toLowerCase()} </p>
              <p> {this.state.email} </p>
            </Col>
            <UpdateUser user={this.state} />
            <DeleteUser id={this.state.id} />
          </Row>
        </Grid>
      )
    }
  }
}

import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import io from 'socket.io-client'
import SmallMuseum from '../museum/SmallMuseum'

export default class FavoritesUser extends Component {
  constructor (props) {
    super(props)
    this.state = {
      favorites: [],
      isLoading: true
    }
    this.socket = io('localhost:8080/favorite')
  }

  componentWillMount () {
    this.socket.emit('GET_ALL_FAVORITES', { id: this.props.id })
    this.socket.on('SEND_ALL_FAVORITES', favorites => {
      let listFavorites = this.state.favorites
      favorites.data.map(favorite => {
        listFavorites.push(favorite)
        return null
      })
      this.setState({
        favorites: listFavorites,
        isLoading: false
      })
    })
  }

  componentDidMount () {
    this.socket.on('RECEIVE_NEW_FAVORITE', fav => {
      this.setState({ favorites: [ ...this.state.favorites, fav ] })
    })
    this.socket.on('DELETE_FAVORITE_OK', fav => {
      console.log(fav.id)
      let list = this.state.favorites.filter(favorite => favorite.id_fav !== fav.id)
      this.setState({
        favorites: list
      })
      console.log(this.state.favorites)
    })
  }

  render () {
    if (this.state.isLoading) {
      return null
    } else {
      let listFavorites = this.state.favorites.map((favorite, index) => {
        return (
          <SmallMuseum name={favorite.name} city={favorite.city} id={favorite.id} url={favorite.url_api} address={favorite.address} id_fav={favorite.id_fav} key={index} />
        )
      })

      return (
        <Grid fluid>
          <Row>
            <Col>
              <h1 className='text-center'> Liste des favoris </h1>
            </Col>
          </Row>
          {listFavorites}
        </Grid>
      )
    }
  }
}

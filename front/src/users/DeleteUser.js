import React, { Component} from 'react'
import { Col, Button } from 'react-bootstrap'
import io from 'socket.io-client'

export default class DeleteUser extends Component {
  constructor (props) {
    super(props)
    this.socket = io('localhost:8080')
  }

  deleteTrigger (e) {
    e.preventDefault()
    this.socket.emit('DELETE_USER', { id: this.props.id })
    this.socket.on('DELETE_USER_OK', () => {
      console.log('User has been deleted')
    })
  }
  render () {
    return (
      <Col>
        <Button onClick={this.deleteTrigger.bind(this)}> Suppression du compte </Button>
      </Col>
    )
  }
}

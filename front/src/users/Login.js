import React, { Component } from 'react'
import io from 'socket.io-client'
import { FormGroup, FormControl, Button, Form, Navbar } from 'react-bootstrap'

export default class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isValide: null
    }
    this.socket = io('localhost:8080')
  }

  handleSubmit (event) {
    event.preventDefault()
    if (this.state.userName.trim().lenght !== 0 && this.state.password.trim().lenght !== 0) {
      this.socket.emit('USER_LOGIN', {
        userName: this.state.userName,
        password: this.state.password
      })
    }
    this.socket.on('USER_LOGIN_ECHEC', () => {
      this.setState({
        isValide: 'error'
      })
    })
    this.socket.on('USER_LOGIN_OK', (userId) => {
      this.setState({
        isValide: 'success'
      })
      this.props.updateUser({ userName: this.state.userName, userId: userId })
    })
  }

  handleChange (event) {
    let field = event.target.name
    let value = event.target.value
    this.setState({
      [field]: value
    })
  }

  ComponentDidMount () {

  }
  render () {
    if (this.state.isValide === 'success') {
      return (
        <Navbar.Text pullRight>{this.state.userName}</Navbar.Text>
      )
    } else {
      return (
        <Navbar.Form pullRight>
          <Form inline onSubmit={this.handleSubmit.bind(this)}>
            <FormGroup bsSize='sm'>
              <FormControl type='text' value={this.state.value} name='userName' placeholder='username' onChange={this.handleChange.bind(this)} />
            </FormGroup>
            <FormGroup bsSize='sm'>
              <FormControl type='password' value={this.state.value} name='password' placeholder='password' onChange={this.handleChange.bind(this)} />
            </FormGroup>
            <Button type='submit' bsSize='sm'>Connexion</Button>
          </Form>
        </Navbar.Form>
      )
    }
  }
}

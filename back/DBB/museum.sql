CREATE TABLE museum (
  id INT PRIMARY KEY NOT NULL,
  region VARCHAR(64) NOT NULL,
  departemant VARCHAR(64) NOT NULL,
  name VARCHAR(125) NOT NULL,
  address TEXT,
  code_postal INT NOT NULL,
  city VARCHAR(256) NOT NULL,
  phone INT,
  web VARCHAR(256),
  annual_closure VARCHAR(256),
  schedule TEXT
)

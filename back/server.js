// Express constantes
const express = require('express')
const app = express()

// http server
const http = require('http').Server(app)

// dependencies
// fetch
const fetch = require('node-fetch')
//  Socket.io
const io = require('socket.io')(http)

// sessions
const session = require('express-session')({
  secret: 'secret',
  name: 'sessionI',
  resave: true,
  saveUninitialized: true
})

const sharedsession = require('express-socket.io-session')

// attach session
app.use(session)

// share with io socket
io.use(sharedsession(session))

// Run the server socket.io
io.on('connection', socket => {
  // print when a new session is start
  console.log(`user connect`)
  console.log(socket.handshake.session)

  // print when the session is close
  socket.on('disconnect', function () {
    console.log('user is disconnect')
  })

  /* ******
   * USER *
   ********/
  // Get a user
  socket.on('USER', function (user) {
    let query = `SELECT * FROM user WHERE id = $id`
    db.get(query, { $id: user.id }, (err, row) => {
      err ? console.log(err) : socket.emit('SEND_USER', { id: row.id, userName: row.user_name, firstName: row.first_name, lastName: row.last_name, password: row.password, email: row.email })
    })
  })

  socket.on('USER_LOGIN', user => {
    let query = `SELECT * FROM user WHERE user_name = $userName AND password = $password`
    db.get(query, { $userName: user.userName, $password: user.password }, (err, row) => {
      if (err) {
        console.log(err)
      } else {
        if (row) {
          socket.emit('USER_LOGIN_OK', row.id, console.log('ok'))
          socket.handshake.session.userdata = row.id
          socket.handshake.session.save()
          console.log(socket.handshake)
        } else {
          socket.emit('USER_LOGIN_ECHEC', console.log('echec'))
        }
      }
    })
  })

  // POST a new user
  socket.on('NEW_USER', data => {
    let query = `SELECT * FROM user WHERE user_name = $userName`
    db.get(query, { $userName: data.userName }, (err, row) => {
      if (err) console.log(err)
      if (row) {
        socket.emit('USER_ALREADY_IN_THE_DB', console.log('user already in the db'))
      } else {
        let query = `INSERT INTO user (last_name, first_name, email, password, user_name) VALUES ($lastName, $firstName, $email, $password, $userName)`
        db.run(query, { $lastName: data.lastName, $firstName: data.firstName, $email: data.email, $password: data.password, $userName: data.userName }, (err) => {
          err ? console.log(err) : socket.emit('RECEIVE_NEW_USER', data, console.log('accpet'))
        })
      }
    })
  })

  // UPDATE user
  socket.on('UPDATE_USER', function (user) {
    let query = `UPDATE user SET last_name = $lastName, first_name = $firstName, email = $email, password = $password, user_name = $userName WHERE id = $id`
    db.run(query, { $id: user.id, $lastName: user.lastName, $firstName: user.firstName, $email: user.email, $password: user.password, $userName: user.userName }, (err) => {
      err ? console.log(err) : socket.emit('UPDATE_USER_OK')
    })
  })

  // DELETE user
  socket.on('DELETE_USER', (user) => {
    let query = `DELETE FROM user WHERE id = $id`
    db.run(query, { $id: user.id }, (err) => {
      err ? console.log(err) : socket.emit('DELETE_USER_OK')
    })
  })
})

/* *************
      Favorites
  ***************/
const fav = io.of('/favorite')
fav.on('connection', socket => {
  // GET favorite
  socket.on('GET_FAVORITE', data => {
    db.get('SELECT * FROM bookmark WHERE id = $id', { $id: data.id }, (err, row) => {
      err ? console.log(err) : socket.emit('SEND_FAVORITE', { data: row })
    })
  })

  // Post favorite
  socket.on('NEW_FAVORITE', data => {
    // check if the favorite is already in the DB
    db.get('SELECT * FROM bookmark WHERE id_user = $user AND id_data = $idData AND url_api = $url;', { $user: data.user, $idData: data.data, $url: data.url }, (err, row) => {
      if (err) console.log(err)
      // if the record exist, send FAVORITE_ALREADY_IN_DB
      if (row) {
        socket.emit('FAVORITE_ALREADY_IN_DB', console.log('Already in the DB'))
      } else {
        // else insert it inside the DB
        db.run('INSERT INTO bookmark (id_user, id_data, url_api) VALUES ( $user, $data, $url )', { $user: data.user, $data: data.data, $url: data.url }, (err, row) => {
          if (err) console.log(err)
          db.get('SELECT b.id AS id_fav, m.id AS id, m.name AS name, m.address AS address, m.city AS city from bookmark AS b LEFT JOIN museum AS m ON b.id_data = m.id WHERE b.id_data = $idData;', { $idData: data.data }, (err, row) => {
            err ? console.log(err) : fav.emit('RECEIVE_NEW_FAVORITE', row)
            console.log('send')
          })
        })
      }
    })
  })

  // DELETE favorite
  socket.on('DELETE_FAVORITE', favorite => {
    db.run('DELETE FROM bookmark WHERE id = $id', { $id: favorite.id }, (err) => {
      err ? console.log(err) : fav.emit('DELETE_FAVORITE_OK', favorite)
    })
  })

  // GET all user's favorites
  socket.on('GET_ALL_FAVORITES', data => {
    db.all('SELECT b.id AS id_fav, m.id AS id, m.name AS name, m.address AS address, m.city AS city from bookmark AS b LEFT JOIN museum AS m ON b.id_data = m.id WHERE b.id_user = $id', { $id: data.id }, (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_ALL_FAVORITES', { data: rows })
    })
  })
})

/* **********
  * MUSEUM *
  **********/
const mus = io.of('/museum')
mus.on('connection', socket => {
  // GET a museum
  socket.on('MUSEUM', museum => {
    db.get('SELECT * from museum WHERE id = $id', { $id: museum.id }, (err, row) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM', row)
    })
  })

  // GET the number of museum per region
  socket.on('MUSEUM_PER_REGION', data => {
    db.all('SELECT count(name) AS nb, region FROM museum GROUP BY region ORDER BY nb DESC', (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM_PER_REGION', rows)
    })
  })

  socket.on('MUSEUM_REG', data => {
    db.all('SELECT count(name) AS nb, departemant FROM museum GROUP BY region, departemant ORDER BY nb DESC', (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM_PER_DEPART', rows)
    })
  })

  // GET museum's nb per departements inside a region
  socket.on('MUSEUM_REG_PER_DEPART', data => {
    db.all(`SELECT count(name) AS nb, departemant FROM museum WHERE region = $region GROUP BY departemant`, {$region: data.region.toUpperCase()}, (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM_REG_PER_DEPART', rows)
    })
  })

  // GET All departemental's museum
  socket.on('MUSEUM_DEPART', data => {
    db.all('SELECT * FROM museum WHERE departemant = $departement', {$departement: data.departement.toUpperCase()}, (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM_DEPART', rows)
    })
  })

  socket.on('MUSEUM_CITIES', data => {
    db.all('SELECT count(name) AS nb, city FROM museum WHERE departemant = $departement GROUP BY city ORDER BY nb DESC', { $departement: data.departement.toUpperCase() }, (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_MUSEUM_CITIES', rows)
    })
  })
  // GET all museum
  socket.on('GET_ALL_MUSEUM', data => {
    db.all('SELECT * from museum LIMIT 20', (err, rows) => {
      err ? console.log(err) : socket.emit('SEND_ALL_MUSEUM', rows)
    })
  })
})

// logs
const morgan = require('morgan')

// parse json
const bodyParser = require('body-parser')

// override method (not necessary)
const methodOverride = require('method-override')

// active cors mode
const cors = require('cors')

// database
const sqlite = require('sqlite3').verbose()

// database museum
let db = new sqlite.Database('DBB/user.db')

// Middlewares
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('__method'))
app.use(morgan('dev'))

// DB functions
let dbQueryAll = (query, res) => {
  db.serialize(() => {
    db.all(query, (err, rows) => {
      if (err) {
        console.log(err)
        res.sendStatus(404)
      } else {
        res.send(rows)
      }
    })
  })
}

// Routes
// GET main route
app.get('/', (req, res) => res.send('Hello world'))

// USERS routes
// Get alls users
app.get('/users', (req, res) => {
  dbQueryAll('SELECT * FROM user', res)
})

// Get one user according the id
app.get('/users/:id', (req, res) => {
  db.serialize(() => {
    let query = `SELECT * FROM user WHERE id = ${req.params.id};`
    db.get(query, (err, row) => {
      err ? res.sendStatus(404) : res.send(row).end()
    })
  })
})

// Add a user
app.post('/users/new', (req, res) => {
  console.log(req.body)
  db.serialize(() => {
    let query = `INSERT INTO user (last_name, first_name, email, password, user_name) VALUES ($lastName, $firstName, $email, $password, $userName)`
    db.run(query, { $lastName: req.body.lastName, $firstName: req.body.firstName, $email: req.body.email, $password: req.body.password, $userName: req.body.userName }, (err) => {
      err ? res.sendStatus(404) : res.sendStatus(201)
    })
  })
})

// Update user's info
app.put('/users/:id', (req, res) => {
  let query = `UPDATE user SET last_name = $lastName, first_name = $firstName, email = $email WHERE id = $id`
  db.run(query, { $lastName: req.body.lastName, $firstName: req.body.firstName, $email: req.body.email, $id: req.params.id }, (err) => {
    err ? res.sendStatus(404) : res.sendStatus(204)
  })
})

// Delete user's info
app.delete('/users/:id', (req, res) => {
  let query = `DELETE FROM user WHERE id = $id`
  db.run(query, { $id: req.params.id }, (err) => {
    err ? res.sendStatus(404) : res.sendStatus(204)
  })
})

// GET all favorites from a user
app.get('/user/:id/favorites/all', (req, res) => {
  db.all('SELECT * from bookmark WHERE id_user = $id', { $id: req.params.id }, (err, rows) => {
    if (err) {
      console.log(err)
      res.sendStatus(404).end()
    } else {
      res.send(rows).end()
    }
  })
})

// Favorites
// GET ALL favorites
app.get('/favorites/all', (req, res) => {
  dbQueryAll('SELECT * FROM bookmark', res)
})

// GET favorites
app.get('/favorites/:id', (req, res) => {
  db.get('SELECT * FROM bookmark WHERE id = $id', { $id: req.params.id }, (err, row) => {
    if (err) {
      console.log(err)
      res.sendStatus(404)
    } else {
      res.send(row)
    }
  })
})

// POST favorites
app.post('/favorites/new', (req, res) => {
  db.run('INSERT INTO bookmark (id_user, id_data, url_api) VALUES ( $user, $data, $url )', { $user: req.body.user, $data: req.body.data, $url: req.body.url }, (err) => {
    if (err) {
      console.log(err)
      res.sendStatus(404)
    } else {
      res.sendStatus(204).end()
    }
  })
})

// Updata favorites
app.put('/favorites/:id', (req, res) => {
  db.run('UPDATE bookmark SET id_data = $data, url_api = $url WHERE id = $id', { $data: req.body.data, $url: req.body.url, $id: req.params.id }, (err) => {
    if (err) {
      console.log(err)
      res.sendStatus(404)
    } else {
      res.sendStatus(200)
    }
  })
})

// DELETE favorites
app.delete('/favorites/:id', (req, res) => {
  db.run('DELETE FROM bookmark WHERE id = $id', { $id: req.params.id }, (err) => {
    let codeStatus = err ? 404 : 200
    res.sendStatus(codeStatus)
  })
})

// Museums routes
app.get('/museum/all', (req, res) => {
  dbQueryAll('SELECT * FROM museum', res)
})

// GET museum's nb per regions
app.get('/museum/regions', (req, res) => {
  db.all('SELECT count(name) AS nb, region FROM museum GROUP BY region ORDER BY nb DESC', (err, rows) => {
    err ? res.sendStatus(404) : res.send(rows).end()
  })
})

// GET museum's nb per departements
app.get('/museum/departements', (req, res) => {
  db.all('SELECT count(name) AS nb, departemant, region FROM museum GROUP BY region, departemant', (err, rows) => {
    err ? res.sendStatus(404) : res.send(rows).end()
  })
})

// GET all museum inside the departement
app.get('/museum/:depart', (req, res) => {
  db.all('SELECT * FROM museum WHERE departemant LIKE $depart', { $depart: req.params.depart }, (err, rows) => {
    err ? res.sendStatus(404) : res.send(rows).end()
  })
})

// GET museum's nb per departements inside a region
app.get('/museum/:region/departements', (req, res) => {
  db.all('SELECT count(name) AS nb, departemant FROM museum WHERE region = $region GROUP BY departemant', {$region: req.params.region.toUpperCase()}, (err, rows) => {
    err ? res.sendStatus(404) : res.send(rows).end()
  })
})

// GET museum
app.get('/museum/:id', (req, res) => {
  let query = `SELECT * FROM museum WHERE id = $id`
  db.get(query, { $id: req.params.id }, (err, row) => {
    err ? res.sendStatus(404) : res.send(row).end()
  })
})

app.get('/:city/coord', (req, res) => {
  fetch(`http://api.openweathermap.org/data/2.5/weather?q=${req.params.city}&units=metric&APPID=0ee0d4b8649b53009c1d1c62a9964138`)
    .then(response => {
      if (response.status !== 200) res.sendStatus(404)
      return response.json()
    })
    .then(myJSON => {
      res.send({
        lat: myJSON.coord.lat,
        lon: myJSON.coord.lon
      }).end()
    })
})

app.get('/:city/activities', (req, res) => {
  fetch(`http://api.openweathermap.org/data/2.5/weather?q=${req.params.city}&units=metric&APPID=0ee0d4b8649b53009c1d1c62a9964138`)
    .then(response => {
      return response.json()
    })
    .then(myJSON => {
      let lat = myJSON.coord.lat
      let long = myJSON.coord.lon
      let key = 'AIzaSyDN1sBjaZj1pSKyADfKdc8IdqSSNe7xiz0'
      let API = 'https://maps.googleapis.com/maps/api/place/nearbysearch'
      fetch(`${API}/json?location=${lat},${long}&radius=500&key=${key}`)
        .then(response => {
          if (response.status !== 200) res.sendStatus(404)
          return response.json()
        })
        .then(myJSON => {
          let results = myJSON.results.map(elt => {
            return {
              name: elt.name,
              type: elt.types
            }
          })
          res.send(results).end()
        })
    })
})

app.get('/:city/:activity', (req, res) => {
  fetch(`http://api.openweathermap.org/data/2.5/weather?q=${req.params.city}&units=metric&APPID=0ee0d4b8649b53009c1d1c62a9964138`)
    .then(response => {
      return response.json()
    })
    .then(myJSON => {
      let lat = myJSON.coord.lat
      let long = myJSON.coord.lon
      let key = 'AIzaSyDN1sBjaZj1pSKyADfKdc8IdqSSNe7xiz0'
      let API = 'https://maps.googleapis.com/maps/api/place/nearbysearch'
      fetch(`${API}/json?location=${lat},${long}&type={req.params.activity}&radius=500&key=${key}`)
        .then(response => {
          if (response.status !== 200) res.sendStatus(404)
          return response.json()
        })
        .then(myJSON => {
          let results = myJSON.results.map(elt => {
            return {
              name: elt.name,
              tyle: elt.types
            }
          })
          res.send(results).end()
        })
    })
})
// Run the server
http.listen(8080, () => console.log('Server running on 8080'))

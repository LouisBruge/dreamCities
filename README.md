# application's project for [Pink Flow](https://www.pingflow.com/)

## description
a small app to stock museum favorites and display museum's distribution by region and departement

## Technologies
JAVASCRIPT :  
  - Node.js
  - React

#### Dependencies
##### back
  - body-parser  
  - cors  
  - express  
  - express-session  
  - express-socket.io-session  
  - method-override  
  - morgan  
  - node-fetch  
  - socket.io  
  - sqlite3

##### front
  - chart.js  
  - react  
  - react-bootstrap  
  - react-chartjs  
  - react-chartjs-2  ,
  - react-dom  
  - react-router  
  - react-router-bootstrap  
  - react-router-dom  
  - react-scripts  
  - reactstrap  
  - socket.io-client  

## USE IT
```bash  
git clone https://gitlab.com/LouisBruge/dreamCities.git
cd dreamCities/back && npm install && npm start
cd ../front && npm install && npm start  
```

## Display
  - Front : [http://localhost:3000/](http://localhost:3000/)
  - Back : [http://localhost:8080/users](http://localhost:8080)

 - 21/05/2018 - V.1.0.0
